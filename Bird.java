package gui_version;

import javafx.scene.image.Image;
import gui_version.MyCanvas;

/**
 * @author Daud
 * Class of Sensor Drone
 */
public class Bird extends Object {

	double birdAngle, birdSpeed;			// angle and speed of travel
	Image drone; //the image of the drone

	public Bird() {
		
	}

	/** 
	 * SensorDrone
	 * Create sensor drone, size ir ay ix,iy, moving at angle ia and speed is
	 * @param ix the x position of the drone
	 * @param iy the y position of the drone
	 * @param ir the radius/size of the drone
	 * @param ia the angle of the movement of the drone
	 * @param is the speed of movement of the drone
	 */
	public Bird(double ix, double iy, double ir, double ia, double is) {
		super(ix, iy, ir);
		birdAngle = ia;
		birdSpeed = is;
		rad = ir;
		drone = new Image(getClass().getResourceAsStream("bird1.png")); //gets the image for this object
	}
	
	/**
	 * drawObject
	 * calls drawDrone
	 */
	@Override
	public void drawObject(MyCanvas mc) {
		drawBird(mc);
	}
	
	/**
	 * @param mc canvas - put the drone in the canvas
	 */
	public void drawBird(MyCanvas mc) {
		mc.drawIt(drone, ax/mc.getXCanvasSize(), by/mc.getYCanvasSize(), rad); //draws the image in the position and size
	}

	/**
	 * checkdrone - change angle of travel if hitting wall or another drone
	 * @param b   droneArena
	 */
	@Override
	protected void checkObject(DroneArena b) {;
		birdAngle = b.CheckDroneAngle(ax, by, rad*2, birdAngle, ObjID);
	}

	/**
	 * adjustdrone
	 * Here, move drone depending on speed and angle
	 */
	@Override
	protected void adjustObject() {
		double radAngle = birdAngle*Math.PI/180;		// put angle in radians
		ax += birdSpeed * Math.cos(radAngle);		// new X position
		by += birdSpeed * Math.sin(radAngle);		// new Y position
	}
	
	/**
	 * saving
	 * string that will store data that will be saved
	 */
	@Override
	public String saving() {
		return (" Bird at: " + ax + " , "+ by + " Angle " + birdAngle + " Speed " + birdSpeed);
	}
	
	/**
	 * getStrType
	 * return string defining drone type
	 */
	protected String getStrType() {
		return "Bird";
	}
	
}
