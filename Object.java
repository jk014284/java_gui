package gui_version;

/**
 * @author Daud
 * abstract class of the objects
 */


public abstract class Object {
	protected double ax, by, rad;						// position and size of Objects
	static int ObjCount = 0;						// used to give each Object a unique identifier
	protected int ObjID;							// unique identifier for object

	Object() {
		this(90, 90, 90);
	}
	
	
	/**
	 * Object
	 * construct a Object of radius ir at ax,by
	 * @param ix x position of the object
	 * @param iy y position of the object
	 * @param ir radius/size of the object
	 */
	
	
	Object (double ix, double iy, double ir) {
		ax = ix;
		by = iy;
		rad = ir;
		ObjID = ObjCount++;			// set the identifier and increment class static
	}
	
	/**
	 * getX
	 * return x position
	 * @return x position
	 */
	
	public double getX() { return ax; }
	
	/**
	 * getY
	 * return y position
	 * @return y position
	 */
	
	public double getY() { return by; }
	
	/**
	 * getRad
	 * return radius 
	 * @return radius/size
	 */
	
	public double getRad() { return rad; }
	/** 
	 * setXY
	 * set the Object at position 
	 * @param nx new x position
	 * @param ny new y position
	 */
	
	public void setXY(double nx, double ny) {
		ax = nx;
		by = ny;
	}
	
	/**
	 * getID
	 * return the identity
	 * @return ID 
	 */
	
	public int getID() {return ObjID; }
	/**
	 * 
	 * drawObject
	 * draw a Object into the canvas
	 * @param mc canvas
	 */
	
	public void drawObject(MyCanvas mc) {
		
	}
	
	/**
	 * getStrType
	 * @return type 
	 */
	
	protected String getStrType() {
		return "Object";
	}
	
	/** 
	 * toString
	 * return string describing Object
	 */
	
	public String toString() {
		return getStrType()+" at "+Math.round(ax)+", "+Math.round(by);
	}
	
	/**
	 * checkObject
	 * abstract method for checking a Object in arena b
	 * @param b drone arena
	 */
	
	protected void checkObject(DroneArena b) {}
	/**
	 * adjustObject
	 * abstract method for adjusting a Object (?moving it)
	 */
	
	protected void adjustObject() {}
	
	/**
	 * is Object at ox,oy size or hitting 
	 * @param ox old x 
	 * @param oy old y
	 * @param or old radius
	 * @return true if hitting
	 */
	
	public boolean hitting(double ox, double oy, double or) {
		return (ox-ax)*(ox-ax) + (oy-by)*(oy-by) < (or+rad)*(or+rad);
	}	
	// hitting if distance between Object and ox,oy < ist rad + or
	
	/** is Object hitting the other Object
	 * 
	 * @param Obj  the other Object
	 * @return true if hitting
	 */
	public boolean hitting (Object obj) {
		return hitting(obj.getX(), obj.getY(), obj.getRad());
	}
	
	/**
	 * saving 
	 * empty to be added to 
	 * @return empty string
	 */
	
	public String saving() {
		return "";
	}
}
