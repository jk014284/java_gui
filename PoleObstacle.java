package gui_version;

import javafx.scene.image.Image;

/**
 * @author Daud
 * obstacle(Pole) for distraction
 */
public class PoleObstacle extends Object {
	
	Image Pole; //image of the obstacle

	public PoleObstacle() {
		
	}

	/**
	 * PoleObstacle
	 * @param ix x position of the Poleobstacle
	 * @param iy y position of the Poleobstacle
	 * @param ir radius/size of the Poleobstacle
	 */
	
	public PoleObstacle(double ix, double iy, double ir) {
		super(ix, iy, ir);
		
		Pole = new Image(getClass().getResourceAsStream("pole1.png")); //gets the image of pole
	}
	
	/**
	 * drawObject
	 * calls drawObstacle
	 */
	@Override
	public void drawObject(MyCanvas mc) {
		drawObstacle(mc);
	}
	
	/**
	 * drawObstacle
	 * @param mc canvas
	 */
	public void drawObstacle(MyCanvas mc) {
		mc.drawIt(Pole, ax/mc.getXCanvasSize(), by/mc.getYCanvasSize(), rad); //draws the obstacle in the x and y position with size
	}

	
	/**
	 * checkObject
	 */
	@Override
	protected void checkObject(DroneArena b) {
		

	}
	
	/**
	 * adjustObject
	 */
	@Override
	protected void adjustObject() {
		

	}
	
	/**
	 * saving
	 * string that will store data that will be saved
	 */
	@Override
	public String saving() {
		return ("Pole at: " + ax + " , "+ by);
	}
	
	/**
	 * getStrType
	 * return string defining drone type
	 */
	protected String getStrType() {
		return "Pole";
	}	

}
