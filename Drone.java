package gui_version;

import javafx.scene.image.Image;
import gui_version.MyCanvas;

/**
 * @author Daud
 * class of normal drone
 */
public class Drone extends Object {

	double droneAngle, droneSpeed;			// angle and speed of travel
	Image drone; //image of the drone

	public Drone() {
		
	}

	/** 
	 * Drone
	 * Create drone, size ir ay ix,iy, moving at angle ia and speed is
	 * @param ix x position of the drone
	 * @param iy y position of the drone
	 * @param ir radius/size of the drone
	 * @param ia angle of movement of the drone
	 * @param is speed of movement of the drone
	 */
	public Drone(double ix, double iy, double ir, double ia, double is) {
		super(ix, iy, ir);
		droneAngle = ia;
		droneSpeed = is;
		rad = ir;
		drone = new Image(getClass().getResourceAsStream("drone.png")); //the image of the drone
	}
	
	/**
	 * drawObject
	 *calls drawDrone
	 */
	@Override
	public void drawObject(MyCanvas mc) {
		drawDrone(mc);
	}
	
	/**
	 * drawDrone
	 * @param mc canvas
	 */
	public void drawDrone(MyCanvas mc) {
		mc.drawIt(drone, ax/mc.getXCanvasSize(), by/mc.getYCanvasSize(), rad); //draws the drone into the canvas in the size and position
	}

	/**
	 * checkObject 
	 * @param b   droneArena
	 */
	@Override
	protected void checkObject(DroneArena b) {;
		droneAngle = b.CheckDroneAngle(ax, by, rad, droneAngle, ObjID); //change the angle of the drone
	}

	/**
	 * adjustObject
	 * Here, move drone depending on speed and angle
	 */
	@Override
	protected void adjustObject() {
		double radAngle = droneAngle*Math.PI/180;		// put angle in radians
		ax += droneSpeed * Math.cos(radAngle);		// new X position
		by += droneSpeed * Math.sin(radAngle);		// new Y position
	}
	
	/**
	* saving
	 * string that will store data that will be saved
	 */
	@Override
	public String saving() {
		return ("Drone at: " + ax + " , "+ by + " Angle " + droneAngle + " Speed " + droneSpeed);
	}
	
	/**
	 * getStrType
	 * return string defining drone type
	 */
	protected String getStrType() {
		return "Drone";
	}
	
}
