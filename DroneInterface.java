package gui_version;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.JFrame;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * @author Daud
 * Example with drones and obstacles in an arena
 */
public class DroneInterface extends Application {
	private MyCanvas canv;
	private AnimationTimer timer;								// timer for animation
	private VBox rtPane;										// vertical box for info
	private DroneArena arena;
	JFileChooser ch = new JFileChooser(); // for file choosing 

	/**
	 * showAbout
	 * function to show in a box ABout the programme
	 */
	
	private void showAbout() {
	    Alert al = new Alert(AlertType.INFORMATION);				// define what box is
	    al.setTitle("About");									// say is About
	    al.setHeaderText(null);
	    al.setContentText("This interface demonstrates the animation of Arena with random drones and obstacles\n You have Add Drone, Add Pole, Add Bird button to add drones and obstacles in random position\n Then start the animation by clicking Start button and stop by clicking stop. ");			// give text
	    al.showAndWait();										// show box and wait for user to close
	}
	
	/**
	 * saveFile
	 * function to choose where to save a file
	 */
	
	private void saveFile() {
		JFrame parentFrame = new JFrame(); //open a new Jfram
		
			ch.setDialogTitle("Choose file");   //outputs to the user
	 
			int userSelection = ch.showSaveDialog(parentFrame);
	 
			if (userSelection == JFileChooser.APPROVE_OPTION) {
				File fileToSave = ch.getSelectedFile();
				System.out.println("Save as file: " + fileToSave.getAbsolutePath()); //the file path the user wants can be found in the menu
				try {
					PrintWriter file = new PrintWriter(new File(fileToSave.getAbsolutePath()));
					
					file.write(arena.savingThis());    		//writes into the file			        					    
					file.close(); //closes the file
					
				} catch (FileNotFoundException e) {
			
					System.out.println("invalid file"); //if the file is invalid
				}
			}
	}
	
	
	
	/**
	 * setMenut
	 * set up the menu of commands for the GUI
	 * @return the menu bar
	 */
	MenuBar setMenu() {
		MenuBar menuBar = new MenuBar();						// create main menu
	
		Menu mFile = new Menu("File");							// add File main menu
		MenuItem mExit = new MenuItem("Exit");					// whose sub menu has Exit
		mExit.setOnAction(new EventHandler<ActionEvent>() {
		    public void handle(ActionEvent t) {					// action on exit is
	        	timer.stop();									// stop timer
		        System.exit(0);									// exit program
		    }
		});
		MenuItem mSave = new MenuItem("Save"); //create the save button
		mSave.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent t) {
				timer.stop();
				saveFile(); //calls save file
			}
		});
		/*
		MenuItem mLoad = new MenuItem("Load Arena");
        mLoad.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                openFile();                        // load arena
            }
        });
		/*
		MenuItem mLoad = new MenuItem("Load");
		mSave.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent t) {
				timer.stop();
				loadFile();
			}
		});*/
		
		mFile.getItems().addAll(mExit, mSave);							// add exit and save and load to File menu
		
		
		Menu mHelp = new Menu("Help");							// create Help menu
		MenuItem mAbout = new MenuItem("Info");				// add About sub men item
		mAbout.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
            	showAbout();									// and its action to print about
            }	
		});
		
		mHelp.getItems().addAll(mAbout);						// add About to Help main item
		
		menuBar.getMenus().addAll(mFile, mHelp);				// set main menu with File, Help
		return menuBar;											// return the menu
	}

	/**
	 * setButtons
	 * set up the horizontal box for the bottom with relevant buttons
	 * @return The HBox
	 */
	
	
	
	private HBox setButtons() {
	    Button btnStart = new Button("Start");					// create button for starting
	    btnStart.setOnAction(new EventHandler<ActionEvent>() {	// now define event when it is pressed
	        @Override
	        public void handle(ActionEvent event) {
	        	timer.start();									// its action is to start the timer
	       }
	    });

	    Button btnStop = new Button("Stop");					// now button for stop
	    btnStop.setOnAction(new EventHandler<ActionEvent>() {
	        @Override
	        public void handle(ActionEvent event) {
	           	timer.stop();									// and its action to stop the timer
	       }
	    });
	    
	    Button btnAdd = new Button("Add Drone");					// now button for adding drone
	    btnAdd.setOnAction(new EventHandler<ActionEvent>() {
	        @Override
	        public void handle(ActionEvent event) { 
	        	arena.addDrone();								//and its action to add
	           	drawWorld();									// 
	       }
	    });

	    
	    Button btnAddObstacle = new Button("Add Pole");				// now button for another obstacle
	    btnAddObstacle.setOnAction(new EventHandler<ActionEvent>() {
	        @Override
	        public void handle(ActionEvent event) {
	           	arena.addObstacle();								// and its action to add obstacle
	           	drawWorld();
	       }
	    });
	    
	    Button btnAddSensorDrone = new Button("Add Bird");				// now button for another sensor drone
	    btnAddSensorDrone.setOnAction(new EventHandler<ActionEvent>() {
	        @Override
	        public void handle(ActionEvent event) {
	           	arena.addSensorDrone();								// and its action to add another sensor drone
	           	drawWorld();
	       }
	    });
	    														// now add these buttons + labels to a HBox
	    return new HBox( btnStart, btnStop, btnAdd, btnAddSensorDrone, btnAddObstacle);
	}

	/** 
	 * drawWorld
	 * draw the world with ball in it
	 */
	public void drawWorld () {
	 	canv.clearCanvas();				
	 	arena.drawArena(canv);
	}
	
	/**
	 * drawStatus
	 * show where ball is, in pane on right
	 */
	public void drawStatus() {
		rtPane.getChildren().clear();					// clear rtpane
		ArrayList<String> allBs = arena.describeAll();
		for (String s : allBs) {
			Label l = new Label(s); 		// turn description into a label
			rtPane.getChildren().add(l);	// add label	
		}	
	}



	/**
	 *
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {
		
		primaryStage.setTitle("DAUD GUI");
	    BorderPane bp = new BorderPane();
	    bp.setPadding(new Insets(10, 20, 10, 20));

	    bp.setTop(setMenu());											// put menu at the top

	    Group root = new Group();										// create group with canvas
	    Canvas canvas = new Canvas( 1000, 700 );
	    root.getChildren().add( canvas );
	    bp.setLeft(root);												// load canvas to left area
	
	    canv = new MyCanvas(canvas.getGraphicsContext2D(), 1000, 700);
	    
	    StackPane holder = new StackPane();
	    holder.getChildren().add(canvas);
	    root.getChildren().add(holder);
	    holder.setStyle("-fx-background-color: white");  // background colour

	    arena = new DroneArena(1000, 700);								// set up arena
	    drawWorld();
	    
	    timer = new AnimationTimer() {									// set up timer
	        public void handle(long currentNanoTime) {					// and its action when on
	        		arena.checkObjects();									// check the angle of all balls
		            arena.adjustObjects();								// move all balls
		            drawWorld();										// redraw the world
		            drawStatus();										// indicate where balls are
	        }
	    };

	    rtPane = new VBox();											// set vBox on right to list items
		rtPane.setAlignment(Pos.CENTER);								// set alignment
		rtPane.setPadding(new Insets(5, 75, 75, 5));					// padding
 		bp.setRight(rtPane);											// add rtPane to borderpane right
		  
	    bp.setBottom(setButtons());										// set bottom pane with buttons

	    Scene scene = new Scene(bp, 1200, 1000);							// set overall scene
        bp.prefHeightProperty().bind(scene.heightProperty());
        bp.prefWidthProperty().bind(scene.widthProperty());

        primaryStage.setScene(scene);
        primaryStage.show();
	  

	}

	/**
	 * main
	 * @param args
	 */
	public static void main(String[] args) {
	    Application.launch(args);			// launch interface

	}

}
